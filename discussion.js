db.course_bookings.insertMany([
	{"courseId": "C001", "studentId": "S004", "isCompleted": true},
	{"courseId": "C002", "studentId": "S001", "isCompleted": false},
	{"courseId": "C001", "studentId": "S003", "isCompleted": true},
	{"courseId": "C003", "studentId": "S002", "isCompleted": false},
	{"courseId": "C001", "studentId": "S002", "isCompleted": true},
	{"courseId": "C004", "studentId": "S003", "isCompleted": false},
	{"courseId": "C002", "studentId": "S004", "isCompleted": true},
	{"courseId": "C003", "studentId": "S007", "isCompleted": false},
	{"courseId": "C001", "studentId": "S005", "isCompleted": true},
	{"courseId": "C004", "studentId": "S008", "isCompleted": false},
	{"courseId": "C001", "studentId": "S013", "isCompleted": true}
]);


/*
	AGGREGATION IN MONGODB
		This is the act or process of generating manipulated data and perform operations to create filtered results that helps in analyzing data
	
	SYNTAX:
		db.collectionName.aggregate([
			{stage 1},
			{stage 2},
			{stage 3}
		])
*/

/*
	$group
	
	SYNTAX:
		{$group: {_id:<id>, fieldResult: "valueResult"}}

*/

db.course_bookings.aggregate([
	{
		$group: {
			_id: null,
			count: {
				$sum: 1
			}
		}
	}
])

/*
	$match

	SYNTAX:
		{
			$match: {
				field: value
			}
		}
*/

db.course_bookings.aggregate([
	{
		$match: {
			"isCompleted": true
		}
	},
	{
		$group: {
			_id: "$courseID",
			total: {
				$sum: 1
			}
		}
	}
])


/*
	$project

	SYNTAX:
		{
			$project: 
				{
					field: 0 or 1
				}
		}
0 to not include in the field to see

1 to include in the field to see
*/

db.course_bookings.aggregate([
	{
		$match: {
			"isCompleted": true
		}	
	},
	{
		$project: {
			"StudentId": 0
		}
	}
])

/*
	$sort

	SYNTAX: 
		{
			$sort: {
				"field": 1 or -1
			}
		}

1 if ascending order

-1 if descending order
*/


db.course_bookings.aggregate([
	{
		$match: {
			"isCompleted": true
		}
	},
	{
		$sort: {
			"courseID": -1
		}
	}
])

db.course_bookings.aggregate([
	{
		$match: {
			"isCompleted": true
		}
	},
	{
		$sort: {
			"StudentId": 1
		}
	},
	{
		$project: {
			"courseID": 1,
			"StudentId": 1,
			"_id": 0
		}
	}
])

/*
	Mini Activity
		1. Count the completed courses of students S013
		2. Sort the courseID in descending order while StudentId in ascending order
*/

db.course_bookings.aggregate([
	{
		$match: {
			"studentId": "S013",
			"isCompleted": true
		}
	},
	{
		$group: {
			"_id": null,
			totalNumberOfCompletedCourse: {
				$sum: 1
			}
		}
	}
])

/*
	$count

	SYNTAX:
		{
			$count: nameOfTheOutputFieldWhichHasItsCountAsItsValue
		}
*/

db.course_bookings.aggregate([
	{
		$match: {
			"studentId": "S013",
			"isCompleted": true
		}
	},
	{
		$count: "totalNumberOfCompletedCourse"
	}	
])


//2. 
db.course_bookings.aggregate([
	{
		$sort: {
			"courseId": -1,
			"studentId":1
		}
	}
])

db.orders.insertMany([
	{
		"cust_Id": "A123",
		"amount": 500,
		"status": "A"
	},
	{
		"cust_Id": "A123",
		"amount": 250,
		"status": "A"
	},
	{
		"cust_Id": "B212",
		"amount": 200,
		"status": "A"
	},
	{
		"cust_Id": "B212",
		"amount": 200,
		"status": "D"
	}
])

db.orders.aggregate([
	{
		$match: {
			"status": "A"
		}
	},
	{
		$group: {
			"_id": "$cust_Id",
			"maxAmount": {
				$max: "$amount"
			}
		}
	}
])

//null lahat masasama
//$columnName means based lang doon sa columns


/*
	$sum operator - will add all (total) the values

	$max operator - will show you the highest value

	$min operator - will show you the lowest value

	$avg operator - will get the average value
*/

//CREATING/SAVING NEW COLLECTION 
db.orders.aggregate([
	{
		$match: {
			"status": "A"
		}
	},
	{
		$group: {
			"_id": "$cust_Id",
			"maxAmount": {
				$max: "$amount"
			}
		}
	},
	{
		$out: "aggregatedResults"
	}
])